public class Car {
    private int fuelLeft;
    private int tankSize;


    public void refuel(int fuel){
        fuelLeft += fuel;

    }

    public Car(int fuelLeft, int tankSize){
        this.fuelLeft = fuelLeft;
        this.tankSize = tankSize;
        //if (fuelLeft > tankSize)
            //this.fuelLeft = tankSize;
    }

    public Car(int fuelLeft) {
        this.fuelLeft = fuelLeft;
    }

    public int getFuelLeft() {
        return fuelLeft;
    }
    public int getTankSize(){
        return tankSize;
    }
}
