import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Scanner;

public class GasStation {
    Scanner sc = new Scanner(System.in);
    NumberFormat formatter = new DecimalFormat("#0.00");
    private double price = 1.11;
    private double gas;

    public void pay(double amount){

        while (true){
            System.out.println("You are due: " + formatter.format(amount));
            System.out.print("Please enter the sum you're paying: ");
            amount -= sc.nextDouble();
            if ((amount >= 0) && (amount < .0001))
                break;
            else if (amount < 0) {
                System.out.println("Here's your change: " + formatter.format(-amount));
                break;
            }
            else if (amount > 0)
                System.out.println("That is not enough.");
        }
        System.out.println("Thank you for using our gas station!");
    }


    public double startStation(Car car){
        System.out.println("Hello at the station!");
        System.out.println("Gas price: " + price);

        int times = 0;

        while (true){

            System.out.println("Current fuel: " + car.getFuelLeft());
            System.out.println("Tank size: " + car.getTankSize());
            System.out.println("Do you want to go on with refuelling? (Y/N)");
            if (sc.nextLine().toLowerCase().equals("n") || car.getFuelLeft()+10> car.getTankSize())
                break;

            car.refuel(10);
            times++;

        }

        return (Double.valueOf(times)*price*10);

    }

    //public void

}

