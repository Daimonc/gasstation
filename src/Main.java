

public class Main {
    public static void main(String[] args) {
        Car car1 = new Car(12,80);
        GasStation station = new GasStation();
        double amountDue = station.startStation(car1);
        station.pay(amountDue);
    }
}
